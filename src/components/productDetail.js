import React, {Component} from "react";
import {Layout, Button, Spin, Carousel, Row, Col} from 'antd';
import HttpRequest from '../util/httpRequest';
import '../pages/homeBase.scss';
import '../pages/teachesPage.scss';
import './detail.scss';
import './productDetail.scss';

const {
  Content,
} = Layout;

export default class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  getDetail(id) {
    let self = this;
    HttpRequest.getProductById(id)
    .then(response => {
      response.data.result.classarr = "ant-row buy ";
  		const newState = Object.assign({}, self.state, {'detail': response.data.result});
  		self.setState(newState);
    })
    .catch(error => {
      console.log(error)
    });
  }
  refresh(){
    this.getDetail()
  }
  componentWillMount(){
  	// 获取详情页信息
  	this.getDetail(this.props.match.params.id.replace(":", ""));
  }
  handleBuyCourse(event) {
    event.stopPropagation();
    event.preventDefault();
    let self = this;
    let detail= self.state.detail;
    detail.classarr = "ant-row buyWapper";
    self.setState({'detail':detail}, () => {
      setTimeout(function (argument) {
        detail.classarr = "ant-row buyWapper buy";
        self.setState({'detail':detail });
      }, 3000);
    }); 
  }
  componentWillUnmount(){
    this.setState({
      detail: false
    })
  }
  render(){
  	if (this.state && this.state.detail) {
      const imgElements = [] 
      let i = 0;
      for (let url of this.state.detail.cover_images) {
        imgElements.push( 
          <div key={i}>
            <img alt="背景图片"  src={url}/>
          </div>
        )
        i++;
      }
  		return (
        <div className="product detail">
          <div className="header">
            <Carousel effect="fade">
              {imgElements}
            </Carousel>
          </div>
          <div className="content HomeBase teaches">
            <Content>
              <div dangerouslySetInnerHTML={{__html: this.state.detail.content}} />
            </Content>
          </div>
          <div className="footer">
            <Button type="primary" onClick={this.handleBuyCourse.bind(this)}>立即购买</Button>
            <Row gutter={24} className={this.state.detail.classarr}>
              <Col span={12}>
                <a href={this.state.detail.link.taobao}><Button type="primary">淘宝</Button></a>
              </Col>
              <Col span={12}>
                <a href={this.state.detail.link.weixin}><Button type="primary">微信</Button></a>
              </Col>
            </Row>
          </div>
        </div>
	  	);
  	} else if(this.state && this.state.detail === false){
      return (
        <div className="loading">
          <Button type="primary" onClick={this.refresh}>点击刷新</Button>
        </div>
      );
  	} else {
      return (
        <div className="loading"><Spin size="large" /></div>
      );
    }
  	
  }
}