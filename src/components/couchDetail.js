import React, {Component} from "react";
import {Layout,  Button, Spin, Carousel, Modal} from 'antd';
import HttpRequest from '../util/httpRequest';
import '../pages/homeBase.scss';
import '../pages/teachesPage.scss';
import './detail.scss';

const {
  Content,
} = Layout;

function info() {
  Modal.info({
    title: '敬请期待',
    content: (
      <div>
        <p>预定/购买功能还在开发中...</p>
      </div>
    ),
    onOk() {},
  });
}

export default class CouchDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  getDetail(id) {
    let self = this;
    HttpRequest.getCouchById(id)
    .then(response => {
  		const newState = Object.assign({}, self.state, {'detail': response.data.result});
  		self.setState(newState);
    })
    .catch(error => {
      console.log(error)
    });
  }
  refresh(){
    this.getDetail()
  }
  componentWillMount(){
  	// 获取详情页信息
  	this.getDetail(this.props.match.params.id.replace(":", ""));
     // <div className="header">
     //        <Carousel effect="fade">
     //          {imgElements}
     //        </Carousel>
     //      </div>
  }
  componentWillUnmount(){
    this.setState({
      detail: false
    })
  }
  render(){
  	if (this.state && this.state.detail) {
      const imgElements = [] 
      let i = 0;
      for (let url of this.state.detail.cover_images) {
        imgElements.push( 
          <div key={i}>
            <img alt="背景图片"  src={url}/>
          </div>
        )
        i++;
      }
  		return (
        <div className="detail couch">
          <div className="content teaches">
            <Content>
              <div dangerouslySetInnerHTML={{__html: this.state.detail.content}} />
            </Content>
          </div>
          <div className="footer">
            <Button type="primary" onClick={info}>立即预约</Button>
          </div>
        </div>
	  	);
  	} else if(this.state && this.state.detail === false){
      return (
        <div className="loading">
          <Button type="primary" onClick={this.refresh}>点击刷新</Button>
        </div>
      );
  	} else {
      return (
        <div className="loading"><Spin size="large" /></div>
      );
    }
  	
  }
}