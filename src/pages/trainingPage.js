import React , {Component} from "react";
import { Link } from 'react-router-dom';
import HttpRequest from '../util/httpRequest';
import './productPage.scss';
import {  Layout, Card, Col, Row, Button, List, Modal, Spin} from 'antd';

const {
  Content,
} = Layout;

const { Meta } = Card;

function info(msg) {
  Modal.info({
    title: '提醒',
    content: msg,
  });
}
/*这是教学内容-在线商城*/

export default class TrainingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollHeight: 0,
      hasMore: true,// 判断接口是否还有数据，通过接口设置
      dataList:[
      ], // 数据列表
      count: 20, 
      offset: 0,
    };
  }

  componentDidMount(){
    this.getCourseList();
  }

  componentWillUnmount(){
    this.setState({
      dataList: []
    })
  }

  // 处理预约事件
  handleClick(index, event){
    event.stopPropagation();
    event.preventDefault();
    document.getElementsByTagName('video')[index].play();
  }

  // 处理滚动监听
  handleScroll(){
    const {hasMore} = this.state;
    console.log(41,hasMore);
    if(!hasMore){
        return;
    }
    console.log('判断前', this.scrollDom.scrollTop, this.scrollDom.clientHeight,  this.scrollDom.scrollHeight);
    //下面是判断页面滚动到底部的逻辑
    if(this.scrollDom.scrollTop + this.scrollDom.clientHeight >= this.scrollDom.scrollHeight ){
      console.log('判断后');
      this.getCourseList()
    }
  }

  getCourseList() {
    let now = new Date() * 1;
    let time = this.time;
    this.setState({
      loading: true
    });
    console.log(time, now);
    if(now && time && (now - time < 2000 ) && this.state.offset !== '0') {
      return;
    } else {
      this.time = now;
    }
    // getLessions
    let courseArray = this.state.dataList;
    let self = this;
    HttpRequest.getTrainings(`?offset=${this.state.offset}&count=${this.state.count}`)
    .then(response => {
      
      for (let item of response.data.result) {
        item.herf = "/detail/training:" + item.id;
        item.subtitle = item.title.split('】')[1];
        item.title = item.title.replace(item.subtitle, "");
        item.autoplay = undefined;
        courseArray.push(item);
      }
      const newState = Object.assign({}, self.state, {
        'offset': self.state.offset + self.state.count,
        'dataList': courseArray,
        'loading': false
      });
      if(response.data.result.length < self.state.count){
        newState.hasMore = false;
      } 
      self.setState(newState);
    })
    .catch(error => {
      console.log(error)
      this.setState({
        loading: false
      });
    });
  }

  render() {

    return (
      <div className="onlineshop scoll" ref={body=>this.scrollDom = body}
                onScroll={this.handleScroll.bind(this)} >
        <div className="icon">
        </div>
        <Content>
          <article className="shadow">
            <List
              grid={{ gutter: 16, column: 3 }}
              dataSource={this.state.dataList}
              loading = {this.state.loading}
              renderItem={(item, i) => (
                <List.Item>
                  <Link to={item.herf}>
                    <Row gutter={24}>
                      <Col span={24}>
                          <Card
                            cover={<video  controls autoplay={item.autoplay}><source src={item.video} type="video/mp4"/></video>}
                          >
                            <Meta
                              title={item.title}
                              description={item.price}
                            />
                            <Button type="primary" onClick={this.handleClick.bind(this,i)}>点击播放</Button>
                          </Card>
                      </Col>
                    </Row>
                  </Link>
                </List.Item>
              )}
            >
            </List>
          </article>
        </Content>
        </div>
  )
  }
}