import React , {Component} from "react";
import HttpRequest from '../util/httpRequest';
import './questionPage.scss';
import $ from  'jquery';
import {   Layout,  Col, Row,  Button,  Checkbox, Input, Modal} from 'antd';

const {
  Content,
} = Layout;


function success() {
  Modal.success({
    title: '发送成功',
    content: '您的信息已经提交到服务器！',
  });
}

function error(msg) {
  Modal.error({
    title: '失败',
    content: msg,
  });
}

function info(msg) {
  Modal.info({
    title: '提醒',
    content: msg,
  });
}

/*这是问卷调查*/
export default class QuestionPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollHeight: 0,
      hasMore: true,// 判断接口是否还有数据，通过接口设置
      data: {
        title: "",
        content: [],
        note: []
      }, // 数据列表
    };
  }

  componentDidMount() {
    // 复选框只能选择一个
  }

  componentWillUnmount(){
    this.setState({
      data: {}
    })
  }

  handleSubmit(e){
    let params = {
      form_id: "form_1",
      content: {

      }
    };
    for (let item of this.state.data.content) {
      let haveflag = false;
      for (let item1 of item.content) {
        if (item1.name) {
          haveflag = true;
          params['content'][item1.name] = $('input[name="' + item1.name + '"]')[0].value
          if (params['content'][item1.name] === undefined || params['content'][item1.name].length === 0) {
            info(`${item1.name}未填写，请填写后提交`);
            return;
          }
        }
      }
      if(haveflag === false  ){
        // 为单选框
        if($('input[name="' + item.name + '"]:checked').length > 1){
          info(`${item.name} 只能选择一个，请重新选择`);
          return;
        }else if($('input[name="' + item.name + '"]:checked')[0]){
          params['content'][item.name] = $('input[name="' + item.name + '"]:checked')[0].value;
        } else {
          info(`${item.name}未选择，请选择后提交`);
          return;
        }
      }
    }
    HttpRequest.setForm(params).then(response => {
      if (response.data.msg === 'ok') {
        success();
      } else {
        error('您的信息提交失败！');
      }
    }).catch(error => {
      error('网络请求失败！')
    });
  }

  componentWillMount(){
    // 获取详情页信息
    this.getForm();
  }

  getForm() {
    let self = this;
    HttpRequest.getFromById('1')
    .then(response => {
      if (response.data.error_code){

      } else {
        const newState = Object.assign({}, self.state, {
          'data': response.data.result
        });
        self.setState(newState);
      }
    })
    .catch(error => {
      console.log(error)
    });
  }

  render() {
    let result = this.state.data;
    return (
      <div className="question joinus">
        <div className="header"></div>
        <Content>
        <div className="ant-advanced-search-form">
            <article className="shadow">
              {
                result.content.map(function(item,index){
                  return (
                      <div className="wrapper" key={'div' + index}>
                        <h3>{item.name + ' ' +item.title}</h3>
                        <Row gutter={16} key={'row' + index} name={item.name}>
                          {
                            item.content.map(function(item1,number){
                              if(item1.type === "input"){
                                return (
                                  <Input placeholder="请填充" key={'input' + number} name={item1.name} addonBefore={item1.name + ' ' +item1.label + ':'}/>
                                );
                              } else if(item1.type === 'select'){
                                return (
                                  <Checkbox.Group key={'checkbox' + number} style={{ width: '100%' }}>
                                    <Row>
                                      {
                                        item1.values.map(function(config, i){
                                          return (
                                            <Col span={8} key={'col' + i}><input type="checkbox"  value={config.value}  name={item.name} /><label>{config.label + ' ' + config.value}</label></Col>
                                          )
                                        })
                                      }
                                    </Row>
                                  </Checkbox.Group>
                                )
                              }
                              return item1;
                            })
                          }
                          
                        </Row>
                        <hr/>
                      </div>
                  );
                })
              }
              <h3>注意事项</h3>
              {
                result.note.map(function(item, index) {
                  return (
                    <p key={'p' + index}>{index+1}.{item.cn}<br/>{item.en}</p>
                  )
                })
              }
              <div className="right">
                <Button type="primary" onClick={this.handleSubmit.bind(this)}>提交</Button>
              </div>
            </article>
          </div>
        </Content>
        </div>
  )
  }
}