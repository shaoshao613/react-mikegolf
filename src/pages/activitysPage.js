import React , {Component} from "react";
import { Link } from 'react-router-dom';
import HttpRequest from '../util/httpRequest';
import {  Layout, Card, Col, Row,  Button, Modal, Spin} from 'antd';
import './homeBase.scss';
import './teachesPage.scss';
import './campsPage.scss';

const {
  Content,
} = Layout;

function info(msg) {
  Modal.info({
    title: '提醒',
    content: msg,
  });
}

/*这是教学内容-教学课程*/
export default class activitysPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollHeight: 0,
      loading: true,
      hasMore: true,// 判断接口是否还有数据，通过接口设置
      dataList:[
      ], // 数据列表
      count: 20, 
      offset: 0,
    };
  }

  componentDidMount(){

      this.getCourseList();
  }

  componentWillUnmount(){
    this.setState({
      dataList: []
    })
  }

  // 处理滚动监听
  handleScroll(){
    const {hasMore} = this.state;
    if(!hasMore){
        return;
    }
    //下面是判断页面滚动到底部的逻辑
    if(this.scrollDom.scrollTop + this.scrollDom.clientHeight >= this.scrollDom.scrollHeight ){
        this.getCourseList()
    }
  }

  
  // 处理预约事件
  handleClick(index, event){
    info('参与活动暂不可用');
    event.stopPropagation();
    event.preventDefault();
  }

  getCourseList() {
    let now = new Date() * 1;
    let time = this.time;
    this.setState({
      loading: true
    });
    console.log(time, now);
    if(now && time && (now - time < 2000 ) && this.state.offset !== '0') {
      return;
    } else {
      this.time = now;
    }
    // getLessions
    let courseArray = this.state.dataList;
    let self = this;
    HttpRequest.getActivitys(`?offset=${this.state.offset}&count=${this.state.count}`)
    .then(response => {
      
      for (let item of response.data.result) {
        item.herf = "/detail/activity:" + item.id;
        item.subtitle = item.title.split('】')[1];
        item.title = item.title.replace(item.subtitle, "");
        courseArray.push(item);
      }
      const newState = Object.assign({}, self.state, {
        'offset': self.state.offset + self.state.count,
        'dataList': courseArray,
        'loading': false
      });
      if(response.data.result.length < self.state.count){
        newState.hasMore = false;
      } 
      self.setState(newState);
    })
    .catch(error => {
      console.log(error)
      this.setState({
        loading: false
      });
    });
  }

  render() {
    let result = this.state.dataList;
    let imgElements = [] // 保存每个用户渲染以后 JSX 的数组
    if (result.length === 0 && this.state.loading === false) {
      imgElements.push(<div className="noData">No data</div>)
    }
    for (let i = 0 ; i < result.length; i ++) {
      if( i % 2 === 0){
        imgElements.push(
          <section className='cardWrapper background-white' key={i}>
            <Link to={result[i].herf}>
              <Row gutter={24}>
                <Col span={12}>
                  <Card className="right-no-padding left"  extra={<div className="subtitle">{result[i].subtitle}</div>} title={result[i].title} bordered={false} >
                    <p className="pwrpper" style = {{'WebkitBoxOrient':'vertical'}} dangerouslySetInnerHTML={{__html: result[i].desc}} ></p>
                    <Button type="primary" onClick={this.handleClick.bind(this,i)}>参与活动</Button>
                  </Card>
                </Col>
                <Col span={12}>
                  <img alt="背景图片"  className='image' src = {result[i].cover_image}/>
                </Col>
              </Row>
            </Link>
          </section>
        );
      } else {
        imgElements.push(
          <section className="cardWrapper background-white" key={i}>
            <Link to={result[i].herf}>
              <Row gutter={24}>
                <Col span={12} className="left-no-padding">
                  <img alt="背景图片"  className='image' src = {result[i].cover_image}/>
                </Col>
                <Col span={12}>
                  <Card  extra={<div className="subtitle">{result[i].subtitle}</div>}  title={result[i].title} bordered={false} className="right">
                    <p className="pwrpper" style = {{'WebkitBoxOrient':'vertical'}} dangerouslySetInnerHTML={{__html: result[i].desc}} ></p>
                    <Button type="primary" onClick={this.handleClick.bind(this,i)}>参与活动</Button>
                  </Card>
                </Col>
              </Row>
            </Link>
          </section>
        );
      }
    }
  	return (
      <div className="HomeBase teaches Camps" ref={body=>this.scrollDom = body}
                onScroll={this.handleScroll.bind(this)}>
        <Content>
      	  <article className="baseinfo" >
            {this.state.loading && this.state.hasMore && (
              <div className="loading">
                <Spin />
              </div>
            )}
      	  	{imgElements}
      	  </article>
        </Content>
        </div>
	)
  }
}