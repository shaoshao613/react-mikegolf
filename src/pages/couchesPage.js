import React , {Component} from "react";
import { Link } from 'react-router-dom';
import HttpRequest from '../util/httpRequest';
import './couchesPage.scss';
import {  Layout, Card, Col, Row, Button, List, Select, Modal, Spin} from 'antd';

const {
  Content,
} = Layout;


function info(msg) {
  Modal.info({
    title: '提醒',
    content: msg,
  });
}

const Option = Select.Option;

/*这是教学内容-教学课程*/

export default class CouchesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollHeight: 0,
      loading: true,
      paramstr: '',
      hasMore: true,// 判断接口是否还有数据，通过接口设置
      dataList:[
      ], // 数据列表
      count: 20, 
      offset: 0,
    };
  }

  componentDidMount(){
    this.getCourseList();
  }

  componentWillUnmount(){
    this.setState({
      dataList: []
    })
  }

  // 处理滚动监听
  handleScroll(){
    const {hasMore} = this.state;
    if(!hasMore){
        return;
    }
    //下面是判断页面滚动到底部的逻辑
    if(this.scrollDom.scrollTop + this.scrollDom.clientHeight >= this.scrollDom.scrollHeight ){
      this.getCourseList()
    }
  }

  // 处理预约事件
  handleClick(index, event){
    info('参与活动暂不可用');
    event.stopPropagation();
    event.preventDefault();
  }

  // 选择下拉框后
  handleChangeLocation(params){
    let newState = {
      dataList: [],
      offset: 0,
      hasMore: true,
      location: params === '0'? undefined: params
    }
    this.setState(newState, () => {
      this.getCourseList();
    });
  }

  handleChangeLevel(params){
    let newState = {
      dataList: [],
      offset: 0,
      hasMore: true,
      level: params === '0'? undefined: params
    }

    this.setState(newState, () => {
      this.getCourseList();
    }); 
  }

  getCourseList() {
    let now = new Date() * 1;
    let time = this.time;
    this.setState({
      loading: true
    });
    console.log(time, now);
    if(now && time && (now - time < 2000 ) && this.state.offset !== '0') {
      return;
    } else {
      this.time = now;
    }
    // getLessions
    let courseArray = this.state.dataList;
    let self = this;
    let url = `?${this.state.paramstr}offset=${this.state.offset}&count=${this.state.count}`;
    if (this.state.level) {
      url = url + '&level=' + this.state.level;
    }
    if (this.state.location) {
      url = url + '&location=' + this.state.location;
    }
    HttpRequest.getCouchs(url)
    .then(response => {
      
      for (let item of response.data.result) {
        item.herf = "/detail/couch:" + item.id;
        item.subtitle = item.title.split('】')[1];
        item.title = item.title.replace(item.subtitle, "");
        courseArray.push(item);
      }
      const newState = Object.assign({}, self.state, {
        'offset': self.state.offset + self.state.count,
        'dataList': courseArray,
        'loading': false
      });
      if(response.data.result.length < self.state.count){
        newState.hasMore = false;
      } 
      self.setState(newState);
    })
    .catch(error => {
      console.log(error)
      this.setState({
        loading: false
      });
    });
  }

  render() {

    return (
      <div className="Couches" ref={body=>this.scrollDom = body}
                onScroll={this.handleScroll.bind(this)} >
        <div className="header">
          <div className="logo"></div>
        </div>
        <Content>
          <div className="filter">
            <div className="selectWrapper">
              <Select defaultValue="级别" style={{ width: '187px' }}  onChange={this.handleChangeLevel.bind(this)} >
                <Option value="">级别</Option>
                <Option value="coach_junior">初级教练</Option>
                <Option value="coach_intermediate">职业教练</Option>
                <Option value="coach_senior">资深职业教练</Option>
                <Option value="president">院长</Option>
              </Select>
              <Select defaultValue="教学点" style={{ width: '187px' }}  onChange={this.handleChangeLocation.bind(this)}>
                <Option value="">教学点</Option>
                <Option value="1">东镇教学总部</Option>
                <Option value="2">黄兴公园教学中心</Option>
                <Option value="3">棕榈滩教学中心</Option>
              </Select>
            </div>
          </div>
          <article className="shadow"  >
            <List
              grid={{ gutter: 9, column: 2 }}
              loading={this.state.loading}
              dataSource={this.state.dataList}
              renderItem={(item, index) => (
                <List.Item>
                  <Link to={item.herf}>
                    <Row gutter={24}>
                      <Col span={8} >
                        <div  className="imageWarpper">
                          <img alt="背景图片"  src={item.cover_image}/>
                        </div>
                      </Col>
                      <Col span={16}>
                        <Card className="right-no-padding left" title={item.name} >
                          <p style = {{'WebkitBoxOrient': 'vertical'}}>{item.name_en}</p>
                          <Button type="primary" onClick={this.handleClick.bind(this, index)}>立即预约</Button>
                        </Card>
                      </Col>
                    </Row>
                  </Link>
                </List.Item>
              )}
            >
            </List>
          </article>
        </Content>
        </div>
  )
  }
}