const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');

module.exports = function override(config, env) {
  // do stuff with the webpack config...
  config = injectBabelPlugin(
  	  ['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }],
  	  config,
  );

  config = rewireLess.withLoaderOptions({
    modifyVars: { 
      "@font-size-base": "12px",
    	"@primary-color": "black" ,
    	"@body-background" : "#fff",
    	"@component-background" : "#fff",
    	"@text-color" : "fade(#000, 65%)"
    },
    javascriptEnabled: true,
  })(config, env);

  return config;
};